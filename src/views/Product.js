import React,{Component} from 'react';

class Product extends Component{
    render(){
        return(
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="current_page">
                                <ul>
                                    <li><a href="/">Woman's Fashion</a></li>
                                    <li><a href="/">Swimsuits</a></li>
                                    <li>2 Piece Swimsuits</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row product_row">
                        <div className="col-lg-7">
                            <div className="product_image">
                                <div className="product_image_large"><img src="images/product_image_1.jpg" alt=""/></div>
                                <div className="product_image_thumbnails d-flex flex-row align-items-start justify-content-start">
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5">
                            <div className="product_content">
                                <div className="product_name">2 Piece Swimsuit</div>
                                <div className="product_price">$35.00</div>
                                <div className="rating rating_4" data-rating="4">
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                </div>
                                <div className="in_stock_container">
                                    <div className="in_stock in_stock_true"></div>
                                    <span>in stock</span>
                                </div>
                                <div className="product_text">
                                    <p>gestas elit iaculis. Prosed placerat felis. Proin non rutrum ligula.</p>
                                </div>
                                <div className="product_quantity_container">
                                    <span>Quantity</span>
                                    <div className="product_quantity clearfix">
                                        <input id="quantity_input" type="text" pattern="[0-9]*" value="1"/>
                                        <div className="quantity_buttons">
                                            <div id="quantity_inc_button" className="quantity_inc quantity_control"><i className="fas fa-caret-up" aria-hidden="true"></i></div>
                                            <div id="quantity_dec_button" className="quantity_dec quantity_control"><i className="fas fa-caret-down" aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="product_size_container">
                                    <span>Size</span>
                                    <div className="product_size">
                                        <ul className="d-flex flex-row align-items-start justify-content-start">
                                            <li>
                                                <input type="radio" id="radio_1" name="product_radio" className="regular_radio radio_1"/>
                                                <label for="radio_1">XS</label>
                                            </li>
                                            <li>
                                                <input type="radio" id="radio_2" name="product_radio" className="regular_radio radio_2" checked/>
                                                <label for="radio_2">S</label>
                                            </li>
                                            <li>
                                                <input type="radio" id="radio_3" name="product_radio" className="regular_radio radio_3"/>
                                                <label for="radio_3">M</label>
                                            </li>
                                            <li>
                                                <input type="radio" id="radio_4" name="product_radio" className="regular_radio radio_4"/>
                                                <label for="radio_4">L</label>
                                            </li>
                                            <li>
                                                <input type="radio" id="radio_5" name="product_radio" className="regular_radio radio_5"/>
                                                <label for="radio_5">XL</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="button cart_button"><a href="/detail">Agregar al carrito</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default Product;