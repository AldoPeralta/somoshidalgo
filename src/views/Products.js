import React,{Component} from 'react';
import {connect} from 'react-redux';
import Globals from '../globals';
import FilterCategories from '../components/FiltersCategories';
import Product from '../components/TargetProduct';

class Productos extends Component{

    state = {
        productList: []
    };

    componentDidMount(){
        this._getCategories();
    }

    componentWillReceiveProps(nextProps){
        this._getCategories(nextProps)
    }

    _getCategories = ({stateGlobal: {categorias, selected_categoria}} = this.props) => {
        this.setState({
            productList: categorias[selected_categoria].productos
        });
    };

    render(){
        return(
            <div className="container" style={{paddingTop:"130px"}}>
                <div className="row justify-content-center">
                    <FilterCategories/>
                    <div className="col-12 col-md-9">
                        <div className="section_title_container text-center" style={{paddingTop:"10px"}}>
                            <div className="section_title">Articulos Encontrados</div>
                            <div className="row products_container justify-content-center">
                                { this.state.productList.map((producto,index) => {
                                    return(
                                        <Product data={producto} key={index}/>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({stateGlobal}) => {
    return {
        stateGlobal: stateGlobal
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setStateGlobal: (data) => {
            return dispatch(Globals.actions.setStateGlobal(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Productos);