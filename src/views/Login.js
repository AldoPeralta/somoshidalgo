import React,{Component} from 'react';

class Login extends Component{
    render(){
        return(
            <div className="container" style={{marginTop:"110px"}}>
                <div className="row justify-content-center">
                    <div className="col-12 col-md-4">
                        <div className="billing">
                            <div className="checkout_title text-center">Iniciar Sesión</div>
                            <div className="checkout_form_container">
                                <form id="checkout_form">
                                    <input type="email" className="checkout_input" placeholder="Email"/>
                                    <input type="text" className="checkout_input" placeholder="Password" required="required"/>
                                    <button className="cart_total_button">Entrar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-4">
                        <div className="billing">
                            <div className="checkout_title text-center">Registrate</div>
                            <div className="checkout_form_container">
                                <form id="checkout_form">
                                    <input type="text" className="checkout_input" placeholder="Nombre"/>
                                    <input type="email" className="checkout_input" placeholder="Email"/>
                                    <input type="password" className="checkout_input" placeholder="Password" required="required"/>
                                    <button className="cart_total_button">Registrar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;