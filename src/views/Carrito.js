import React,{Component} from 'react';

class Carrito extends Component{

    state = {
        products: [
            {
                name: "Dicos",
                price: 235,
                quantity: 3
            },
            {
                name: "Libros",
                price: 250,
                quantity: 7
            },
            {
                name: "Artesania",
                price: 175,
                quantity: 11
            }
        ]
    }
    render(){
        return(
            <div className="cart_container">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="cart_title">Tu carrito de compras</div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="cart_bar d-flex flex-row align-items-center justify-content-start">
                                <div className="cart_bar_title_name">Producto</div>
                                <div className="cart_bar_title_content ml-auto">
                                    <div className="cart_bar_title_content_inner d-flex flex-row align-items-center justify-content-end">
                                        <div className="cart_bar_title_price">Precio</div>
                                        <div className="cart_bar_title_quantity">Cantidad</div>
                                        <div className="cart_bar_title_total">Total</div>
                                        <div className="cart_bar_title_button"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="cart_products">
                                <ul>
                                    {this.state.products.map((item,index) => {
                                        return(
                                            <li className="cart_product d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start" key={index}>
                                                <div className="cart_product_image"><img src="images/cart_product_1.jpg" alt=""/></div>
                                                <div className="cart_product_name"><a href="product.html">{item.name}</a></div>
                                                <div className="cart_product_info ml-auto">
                                                    <div className="cart_product_info_inner d-flex flex-row align-items-center justify-content-md-end justify-content-start">
                                                        <div className="cart_product_price">{item.price}</div>
                                                        <div className="product_quantity_container1">
                                                            <div className="product_quantity clearfix">
                                                                <input id="quantity_input" type="text" pattern="[0-9]*" value={item.quantity}/>
                                                                <div className="quantity_buttons">
                                                                    <div id="quantity_inc_button" className="quantity_inc quantity_control"><i className="fas fa-caret-up" aria-hidden="true"></i></div>
                                                                    <div id="quantity_dec_button" className="quantity_dec quantity_control"><i className="fas fa-caret-down" aria-hidden="true"></i></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="cart_product_total">${item.price * item.quantity}</div>
                                                        <div className="cart_product_button">
                                                            <button className="cart_product_remove"><img src="images/trash.png" alt=""/></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="cart_control_bar d-flex flex-md-row flex-column align-items-start justify-content-start">
                                <button className="button_clear cart_button">Vaciar Carrito</button>
                                <button className="button_update cart_button">Actualizar Carrito</button>
                                <button className="button_update cart_button_2 ml-md-auto">Continuar comprando</button>
                            </div>
                        </div>
                    </div>
                    <div className="row cart_extra">}
                        <div className="col-lg-6">
                            <div className="cart_coupon">
                                <div className="cart_title">Cupón de Descuento</div>
                                <form action="#" className="cart_coupon_form d-flex flex-row align-items-start justify-content-start" id="cart_coupon_form">
                                    <input type="text" className="cart_coupon_input" placeholder="Coupon code" required="required"/>
                                    <button className="button_clear cart_button_2">User cupon</button>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-5 offset-lg-1">
                            <div className="cart_total">
                                <div className="cart_title">Compra Total</div>
                                <ul>
                                    <li className="d-flex flex-row align-items-center justify-content-start">
                                        <div className="cart_total_title">Subtotal</div>
                                        <div className="cart_total_price ml-auto">$35.00</div>
                                    </li>
                                    <li className="d-flex flex-row align-items-center justify-content-start">
                                        <div className="cart_total_title">I.V.A</div>
                                        <div className="cart_total_price ml-auto">$5.00</div>
                                    </li>
                                    <li className="d-flex flex-row align-items-center justify-content-start">
                                        <div className="cart_total_title">Total</div>
                                        <div className="cart_total_price ml-auto">$40.00</div>
                                    </li>
                                </ul>
                                <button className="cart_total_button">Proceder al pago</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Carrito;