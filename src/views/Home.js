import React,{Component} from 'react';
import Slider from '../components/Slider';
import Promos from '../components/Promos';
import NewProducts from '../components/NewProducts';

class Home extends Component{
    render(){
        return(
            <div>
                <Slider/>
                <Promos/>
                <NewProducts/>
            </div>
        );
    }
}

export default Home;