import React,{Component} from 'react';

class Musica extends Component{

    state = {
        products:[
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco1.jpg"
            },
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco2.jpg"
            },
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco3.jpg"
            },
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco1.jpg"
            },
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco2.jpg"
            },
            {
                name:"Disco",
                price:"350",
                imgUrl:"images/disco3.jpg"
            }
        ]
    }

    render(){
        return(
            <div className="container" style={{paddingTop:"130px"}}>
                <div className="row justify-content-center">
                    <div className="col-12 col-md-3">
                        <div className="section_title_container text-center">
                            <div className="section_title">Dependencias</div>
                            <div class="list-group">
                                <a href="/musica" class="list-group-item list-group-item-action">CECULTAH</a>
                                <a href="/musica" class="list-group-item list-group-item-action">Independiente</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-9">
                        <div className="section_title_container text-center" style={{paddingTop:"10px"}}>
                            <div className="section_title">Discos</div>
                            <div className="section_subtitle">Articulos Encontrados</div>
                            <div className="row products_container justify-content-center">
                                {this.state.products.map((item,index) => {
                                    return(
                                        <div className="col-lg-4 product_col" key={index} style={{paddingBottom:"10px"}}>
                                            <div className="producto">
                                                <div className="product_image">
                                                    <img src={item.imgUrl} alt=""/>
                                                </div>
                                                <div className="product_content clearfix">
                                                    <div className="product_info">
                                                        <div className="product_name"><a href="product.html">{item.name}</a></div>
                                                        <div className="product_price">${item.price}</div>
                                                    </div>
                                                    <div className="product_options">
                                                        <div className="product_buy product_option"><img src="images/shopping-bag-white.svg" alt=""/></div>
                                                        <div className="product_fav product_option">+</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Musica;