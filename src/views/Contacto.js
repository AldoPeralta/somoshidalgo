import React,{Component} from 'react';

class Contacto extends Component{
    render(){
        return(
            <div className="container" style={{paddingTop:"110px"}}>
                <div className="row justify-content-center">
                    <div className="col-12 col-md-8">
                        <div className="accordion" id="accordionExample">
                            <div className="card">
                                <div className="card-header text-center" id="headingOne">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Pachuca de Soto
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" 
                                                    style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/pachucadetalle.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Cuartel del Arte, Plaza Aniceto Ortega S/N, Centro Historico.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d936.5773236167662!2d-98.73248637201371!3d20.121140115079083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d109e405a01129%3A0x83f44ab9bd23333f!2sEl+Cuartel+del+Arte!5e0!3m2!1ses-419!2smx!4v1559161895670!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Pachuca"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header text-center" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Tulancingo de Bravo
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" 
                                                    style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/tulancingodetalle.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                            Centro Cultural "Ricardo Garibay", Jardin del Arte S/N, Colonia Centro
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d936.8117429338267!2d-98.36997447201611!3d20.081960316374595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d056f5d400ee4d%3A0xd61e38a051db89ae!2sCentro+Cultural+Ricardo+Garibay!5e0!3m2!1ses-419!2smx!4v1559173310293!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Tulancingo"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="card">
                                <div className="card-header text-center" id="headingThree">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Ixmiquilpan
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/ixmiquilpan.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Centro Cultural del Valle del Mezquital, Av. Insurgentes S/N, Colonia Centro
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d119666.23144895455!2d-99.31423993928888!3d20.400622915004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d3e0701ad8fc79%3A0xf017663cbfd84eb2!2sCentro+Cultural+del+Valle+del+Mezquital.+Museo+de+la+Cultura+H%C3%B1ah%C3%B1u%2C+Punto+de+venta+y+esc.+de+Artes!5e0!3m2!1ses-419!2smx!4v1559173437309!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Ixmiquilpan"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <div className="card">
                                <div className="card-header text-center" id="headingSix">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            Mineral del Monte
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" className="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/mineraldelmonte.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Centro Cultural, Calle Teodoro Manzano No. 1, Centro Histórico
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3745.841683299514!2d-98.67463408577!3d20.140651422878307!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d10812ca82d4cb%3A0xafab59cd65d1e71d!2sCentro+Cultural+de+Real+de+Monte!5e0!3m2!1ses-419!2smx!4v1559173572425!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Mineral del Monte"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header text-center" id="headingFive">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            Tepeapulco
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" className="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/tepeapulcodetalle.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Kiosco de la Plaza de la Constitución S/N, Colonia Centro
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d469.2851456756499!2d-98.55451400631691!3d19.785672754679585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1b4343b7d6eff%3A0xb8c30352be139dc4!2sPresidencia+Municipal+de+Tepeapulco!5e0!3m2!1ses-419!2smx!4v1559173807438!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Tepeapulco"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header text-center" id="headingFour">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Tecozautla
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/tecozautladetalle.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Plaza Constitución No. 1, Centro Histórico
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d467.03967649913375!2d-99.63451477593736!3d20.53392760328671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x22910d17aa729dbc!2sPlaza+de+la+Constituci%C3%B3n%2FTorre%C3%B3n+Monumental!5e0!3m2!1ses-419!2smx!4v1559174034464!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Tecozautla"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header text-center" id="heading7">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                            Tula de Allende
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapse7" className="collapse" aria-labelledby="heading7" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/tuladetalle.jpg) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Sala Histórica Quetzalcóatl, Calle Ignacio Zaragoza S/N, Colonia Centro
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.866500953573!2d-99.34489558577138!3d20.056033725648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d22d14feb4cb85%3A0x6b2025b3d6d2cc2f!2sSala+Hist%C3%B3rica+Quetzalc%C3%B3atl!5e0!3m2!1ses-419!2smx!4v1559174743756!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Tula de Allende"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header text-center" id="heading8">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                            Huichapan
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapse8" className="collapse" aria-labelledby="heading8" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="jumbotron jumbotron-fluid row align-items-center" style={{background: "url(http://cdn.hidalgo.gob.mx/estado/img/municipios/huichapandetalle.png) 0% 0% / cover"}}>
                                                        <div className="container" id="image-text">
                                                            <p className="lead" style={{fontSize:"24px"}} id="lead">
                                                                Calle M. Chávez Nava No. 12, Centro Histórico
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <iframe 
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d935.0445284787314!2d-99.64959187083963!3d20.37554475634022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d3bc3db3932b19%3A0x536feb3140363812!2sM+Ch%C3%A1vez+Nava+12%2C+Centro+Hist%C3%B3rico%2C+42400+Huichapan%2C+Hgo.!5e0!3m2!1ses-419!2smx!4v1559175082174!5m2!1ses-419!2smx" 
                                                        width="100%" 
                                                        height="350" 
                                                        title="Huichapan"
                                                        frameborder="0" style={{border:"0"}} allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contacto;