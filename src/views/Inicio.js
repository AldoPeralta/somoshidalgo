import React, {Component} from 'react';
import {connect} from 'react-redux';
import Globals from '../globals';
import Products from './Products';

class Inicio extends Component{
    componentDidMount(){
        const categoria = window.location.pathname.split(window.carpeta + "/").join("").toUpperCase();
        console.log(categoria);
        this.props.setStateGlobal({selected_categoria: categoria});
    }

    render(){
        return(
            <div>
                <Products/>
            </div>
        );
    }
};

const mapStateToProps = ({stateGlobal}) => {
    return {
        stateGlobal: stateGlobal
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setStateGlobal: (data) => {
            return dispatch(Globals({}).actions.setStateGlobal(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Inicio);