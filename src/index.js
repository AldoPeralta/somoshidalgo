import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {createStore,combineReducers} from 'redux';
import {Provider} from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import Globals from './globals';

Promise.all([
    axios.post(window.url_api + '/catalogos/artesanias.php', {}),
    axios.post(window.url_api + '/catalogos/libros.php', {}),
    axios.post(window.url_api + '/catalogos/musica.php', {})
])
.then(Startx)
.catch(reason => {
    console.log(reason);  
});

function Startx(response) {
    console.log(response);
    const init = Globals({
        artesanias: response[0].data,
        libros: response[1].data,
        musica: response[2].data
    });

    const store = createStore(combineReducers(init.states));

    ReactDOM.render(
        <Provider store={store}>
            <Router>
                <App/>
            </Router>
        </Provider>
    , document.getElementById('root'));
};
