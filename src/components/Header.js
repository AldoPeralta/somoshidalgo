import React,{Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component{
     render(){
         return(
             <div className="super_container">
                 <header className="header">
                    <div className="header_inner d-flex flex-row align-items-center justify-content-start">
                        <div className="logo">
                            <Link to="/">
                                <div className="logosecretaria"></div>
                            </Link>
                        </div>
                        <nav className="main_nav">
                            <ul>
                                <li>
                                    <div className="dropdown">
                                        <a className="dropdown-toggle" href="/artesanias" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Artesanias
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a className="dropdown-item" href="/artesanias">Alfarería</a>
                                            <a className="dropdown-item" href="/artesanias">Metalistería</a>
                                            <a className="dropdown-item" href="/artesanias">Gastronomía Artesanal Hidalguense</a>
                                            <a className="dropdown-item" href="/artesanias">Fibras Vegetales Duras y Semiduras</a>
                                            <a className="dropdown-item" href="/artesanias">Alfarría y Cerámica</a>
                                            <a className="dropdown-item" href="/artesanias">Incrustación de Concha de De Abulón en Madera</a>
                                            <a className="dropdown-item" href="/artesanias">Cantería y Lapidaría</a>
                                            <a className="dropdown-item" href="/artesanias">Incrustación de Concha de Abulón en Alpaca</a>
                                            <a className="dropdown-item" href="/artesanias">Madera</a>
                                            <a className="dropdown-item" href="/artesanias">Textil</a>
                                            <a className="dropdown-item" href="/artesanias">Tejido y Torcido de Fibras Vegetales Duras y Semiduras</a>
                                            <a className="dropdown-item" href="/artesanias">Gastronomia Diferentes Ingredientes</a>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="/gastronomia">Gastronomia</a></li>
                                <li><a href="/literatura">Literatura</a></li>
                                <li><a href="/musica">Musica</a></li>
                                <li><Link to="/contacto">Contacto</Link></li>
                                <li><a href="/login">Acceso</a></li>
                            </ul>
                        </nav>
                        <div className="header_content ml-auto">
                            <div className="shopping">
                                <a href="/carrito">
                                    <div className="cart">
                                        <img src="images/shopping-bag.svg" alt=""/>
                                        <div className="cart_num_container">
                                            <div className="cart_num_inner">
                                                <div className="cart_num">1</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="/checkout">
                                    <div className="star">
                                        <img src="images/star.svg" alt=""/>
                                        <div className="star_num_container">
                                            <div className="star_num_inner">
                                                <div className="star_num">0</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="/login">
                                    <div className="avatar">
                                        <img src="images/avatar.svg" alt=""/>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div className="burger_container d-flex flex-column align-items-center justify-content-around menu_mm"><div></div><div></div><div></div></div>
                    </div>
                </header>

                <div className="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
                    <div className="menu_close_container"><div className="menu_close"><div></div><div></div></div></div>
                    <div className="logo menu_mm"><img src="images/logo_o.png" style={{maxWidth:"90%",height:"auto"}} alt=""/></div>
                    <div className="search">
                        <form action="/">
                            <input type="search" className="search_input menu_mm" required="required"/>
                            <button type="submit" id="search_button_menu" className="search_button menu_mm"><img className="menu_mm" src="images/magnifying-glass.svg" alt=""/></button>
                        </form>
                    </div>
                    <nav className="menu_nav">
                        <ul className="menu_mm">
                            <li>
                                <div className="dropdown">
                                    <a className="dropdown-toggle" href="/artesanias" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Artesanias
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a className="dropdown-item" href="/artesanias">Alfarería</a>
                                        <a className="dropdown-item" href="/artesanias">Metalistería</a>
                                        <a className="dropdown-item" href="/artesanias">Gastronomía Artesanal Hidalguense</a>
                                        <a className="dropdown-item" href="/artesanias">Fibras Vegetales Duras y Semiduras</a>
                                        <a className="dropdown-item" href="/artesanias">Alfarría y Cerámica</a>
                                        <a className="dropdown-item" href="/artesanias">Incrustación de Concha de De Abulón en Madera</a>
                                        <a className="dropdown-item" href="/artesanias">Cantería y Lapidaría</a>
                                        <a className="dropdown-item" href="/artesanias">Incrustación de Concha de Abulón en Alpaca</a>
                                        <a className="dropdown-item" href="/artesanias">Madera</a>
                                        <a className="dropdown-item" href="/artesanias">Textil</a>
                                        <a className="dropdown-item" href="/artesanias">Tejido y Torcido de Fibras Vegetales Duras y Semiduras</a>
                                        <a className="dropdown-item" href="/artesanias">Gastronomia Diferentes Ingredientes</a>
                                    </div>
                                </div>
                                </li>
                            <li className="menu_mm"><a href="/gastronomia">Gastronomia</a></li>
                            <li className="menu_mm"><a href="/literatura">Literatura</a></li>
                            <li className="menu_mm"><a href="/musica">Musica</a></li>
                            <li className="menu_mm"><Link to="/contacto">Contacto</Link></li>
                            <li className="menu_mm"><a href="/login">Acceder</a></li>
                        </ul>
                    </nav>
                </div>
             </div>
         );
     }
 }

 export default Header;