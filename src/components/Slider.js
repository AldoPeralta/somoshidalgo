import React,{Component} from 'react';

class Slider extends Component{
    render(){
        return(
            <div className="home">
                <div className="home_slider_container">
                    <div className="owl-carousel owl-theme home_slider">
                        <div className="owl-item">
                            <div className="home_slider_background" style={{backgroundImage:"url(images/tienda1.jpg)"}}></div>
                            <div className="home_slider_content">
                                <div className="home_slider_content_inner">
                                    <div className="home_slider_title">#SomosHidalgo</div>
                                </div>	
                            </div>
                        </div>
                        <div className="owl-item">
                            <div className="home_slider_background" style={{backgroundImage:"url(images/tienda2.jpg)"}}></div>
                            <div className="home_slider_content">
                                <div className="home_slider_content_inner">
                                    <div className="home_slider_title">#SomosHidalgo</div>
                                </div>	
                            </div>
                        </div>
                        <div className="owl-item">
                            <div className="home_slider_background" style={{backgroundImage:"url(images/tienda3.jpg)"}}></div>
                            <div className="home_slider_content">
                                <div className="home_slider_content_inner">
                                    <div className="home_slider_title">#SomosHidalgo</div>
                                </div>	
                            </div>
                        </div>
                    </div>

                    <div className="home_slider_next d-flex flex-column align-items-center justify-content-center"><img src="images/arrow_r.png" alt=""/></div>

                    <div className="home_slider_dots_container">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="home_slider_dots">
                                        <ul id="home_slider_custom_dots" className="home_slider_custom_dots">
                                            <li className="home_slider_custom_dot active"><div></div></li>
                                            <li className="home_slider_custom_dot"><div></div></li>
                                            <li className="home_slider_custom_dot"><div></div></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>		
                    </div>
                </div>
            </div>
        );
    }
}

export default Slider;