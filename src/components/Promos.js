import React,{Component} from 'react';

class Promos extends Component{
    render(){
        return(
            <div className="promo">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="section_title_container text-center">
                                <div className="section_subtitle">Unicamente lo mejor</div>
                                <div className="section_title">Promociones</div>
                            </div>
                        </div>
                    </div>
                    <div className="row promo_container">
                        <div className="col-lg-4 promo_col">
                            <div className="promo_item">
                                <div className="promo_image">
                                    <img src="images/art1.jpg" alt=""/>
                                    <div className="promo_content promo_content_2">
                                        <div className="promo_title">-30% OFF</div>
                                        <div className="promo_subtitle">En todas las bolsas</div>
                                    </div>
                                </div>
                                <div className="promo_link"><a href="/">Comprar Ahora</a></div>
                            </div>
                        </div>
                        <div className="col-lg-4 promo_col">
                            <div className="promo_item">
                                <div className="promo_image">
                                    <img src="images/art2.jpg" alt=""/>
                                    <div className="promo_content promo_content_2">
                                        <div className="promo_title">-30% OFF</div>
                                        <div className="promo_subtitle">Abrigos y Chaquetas</div>
                                    </div>
                                </div>
                                <div className="promo_link"><a href="/">Comprar Ahora</a></div>
                            </div>
                        </div>
                        <div className="col-lg-4 promo_col">
                            <div className="promo_item">
                                <div className="promo_image">
                                    <img src="images/art3.jpg" alt=""/>
                                    <div className="promo_content promo_content_2">
                                        <div className="promo_title">-25% OFF</div>
                                        <div className="promo_subtitle">En Sandalias</div>
                                    </div>
                                </div>
                                <div className="promo_link"><a href="/">Comprar Ahora</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Promos;