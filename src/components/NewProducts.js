import React,{Component} from 'react';

class NewProducts extends Component{
    render(){
        return(
            <div className="arrivals">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="section_title_container text-center">
                                <div className="section_subtitle">Unicamente lo mejor</div>
                                <div className="section_title">Nuevos Productos</div>
                            </div>
                        </div>
                    </div>
                    <div className="row products_container">
                        <div className="col-lg-4 product_col">
                            <div className="producto">
                                <div className="product_image">
                                    <img src="images/art4.jpg" alt=""/>
                                </div>
                                <div className="rating rating_4">
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                </div>
                                <div className="product_content clearfix">
                                    <div className="product_info">
                                        <div className="product_name"><a href="product.html">Vestido Largo de Dama</a></div>
                                        <div className="product_price">$45.00</div>
                                    </div>
                                    <div className="product_options">
                                        <div className="product_buy product_option"><img src="images/shopping-bag-white.svg" alt=""/></div>
                                        <div className="product_fav product_option">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 product_col">
                            <div className="producto">
                                <div className="product_image">
                                    <img src="images/art5.jpg" alt=""/>
                                </div>
                                <div className="rating rating_4">
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                </div>
                                <div className="product_content clearfix">
                                    <div className="product_info">
                                        <div className="product_name"><a href="product.html">Traje de baño</a></div>
                                        <div className="product_price">$35.00</div>
                                    </div>
                                    <div className="product_options">
                                        <div className="product_buy product_option"><img src="images/shopping-bag-white.svg" alt=""/></div>
                                        <div className="product_fav product_option">+</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 product_col">
                            <div className="producto">
                                <div className="product_image">
                                    <img src="images/art6.jpg" alt=""/>
                                </div>
                                <div className="rating rating_4">
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="fas fa-star"></i>
                                </div>
                                <div className="product_content clearfix">
                                    <div className="product_info">
                                        <div className="product_name"><a href="product.html">Chamarra Azul de Hombre</a></div>
                                        <div className="product_price">$145.00</div>
                                    </div>
                                    <div className="product_options">
                                        <div className="product_buy product_option"><img src="images/shopping-bag-white.svg" alt=""/></div>
                                        <div className="product_fav product_option"><a href="/detail">+</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewProducts;