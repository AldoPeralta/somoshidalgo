import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Globals from '../globals';

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d === undefined ? "." : d,
      t = t === undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;
  
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

class TargetProduct extends Component{
    _addToStorage = (stock,obj) => {
        const set_obj = {};
        set_obj[stock] = [...this.props.stateGlobal[stock], ...[obj]];
        this.props.setStateGlobal(set_obj);
        window.localStorage.setItem(stock,JSON.stringify(set_obj[stock]));
    };

    render(){
        const {
            no,codigo,image,costo,nombre,titulo
        } = this.props.data;

        return(
            <div className="col-lg-4 product_col" style={{paddingBottom:"25px"}}>
                <div className="producto">
                    <div className="product_image">
                        <img src={window.url_api + "/images/artesanias/" + codigo + "/1.png"} alt="" style={{maxHeight: 150, minHeight: 150}}/>
                    </div>
                    <div className="product_content clearfix">
                        <div className="product_name" style={{width:"100%"}}>
                            <a className="text-center" href="#">{nombre || titulo}</a>
                        </div>
                        <div className="product_info">
                            <div className="product_price">${ formatMoney(costo) }</div>
                        </div>
                        <div className="product_options">
                            <div 
                                className="product_buy product_option"
                                style={{cursor: "pointer"}}
                                onClick={() => {
                                    this._addToStorage("lista_de_compras",this.props.data);
                                    window.alert("Se agrego al carrito de compras.");
                                }}>
                                <img src="images/shopping-bag-white.svg" alt=""/>
                            </div>
                            <div className="product_fav product_option">Ver</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({stateGlobal}) => {
    return {
        stateGlobal: stateGlobal
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setStateGlobal: (data) => {
            return dispatch(Globals.actions.setStateGlobal(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(TargetProduct);