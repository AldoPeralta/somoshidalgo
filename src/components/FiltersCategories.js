import React,{Component} from 'react';
import {connect} from 'react-redux';
import Globals from '../globals';

class FilterCategories extends Component{
    state = {
        subcategorias: [],
        materiales: []
    };

    componentDidMount(){
        this._getCategories();
    }

    componentWillReceiveProps(nextProps){
        this._getCategories(nextProps);
    }

    _getCategories = ({stateGlobal: {selected_categoria,categorias}} = this.props) => {
        this.setState({
            subcategorias: categorias[selected_categoria].subcategorias,
            materiales: categorias[selected_categoria].materiales
        });
    };

    render(){
        return(
            <div className="col-12 col-md-3">
                <div className="section_title_container text-center">
                    <div className="section_title">Categorias</div>
                    <div className="list-group">
                        {
                            this.state.subcategorias.map((text) => {
                                return this._renderItemMenu(text);
                            })
                        }
                    </div>
                </div>
                <div className="section_title_container text-center">
                    <div className="section_title">Materiales</div>
                    <div className="list-group">
                        {
                            this.state.materiales.map((text,index) => {
                                return this._renderItemMenu(text);
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }

    _renderItemMenu = (name,index) => {
        return(
            <a href="/artesanias" key={index} className="list-group-item list-group-item-action">{name}</a>
        );
    }
}

const mapStateToProps = ({stateGlobal}) => {
    return {
        stateGlobal: stateGlobal
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setStateGlobal: (data) => {
            return dispatch(Globals.actions.setStateGlobal(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(FilterCategories);