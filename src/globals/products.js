export default [
{
"name":"MANTEL DE 30X30 DE IXTLE",
"description":"ELABORADO EN IXTLE NATURAL EN TELAR DE CINTURA",
"price":51.60,
"uuid":"4010",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 1.jpg",
"piezas":24
},
{
"name":"GUANTE CORPORAL",
"description":"TEJIDO A MANO EN IXTLE NATURAL",
"price":90.00,
"uuid":"4002",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 2.jpg",
"piezas":7
},
{
"name":"BOLSA DE MANO 300",
"description":"TEJIDO A MANO EN IXTLE TEÑIDO CON TINTES NATURALES",
"price":360.00,
"uuid":"4013",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 3.jpg",
"piezas":1
},
{
"name":"BOLSA DE MANO 350",
"description":"TEJIDO A MANO EN IXTLE TEÑIDO CON TINTES NATURALES",
"price":420.00,
"uuid":"4012",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 4.jpg",
"piezas":1
},
{
"name":"JUEGO COLLAR Y ARETES DE IXTLE",
"description":"TEJIDOS A MANO EN IXTLE TEÑIDOS CON TINTES NATURALES",
"price":144.00,
"uuid":"13005",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 5.jpg",
"piezas":3
},
{
"name":"CRUZ TRIANGULAR CONCHA DE ABULÓN",
"description":"ELABORADA EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":420.00,
"uuid":"3009",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 6.jpg",
"piezas":5
},
{
"name":"COLECCIÓN 14 INSTRUMENTOS - CONCHA DE ABULÓN",
"description":"ELABORADA EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":1680.00,
"uuid":"3008",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 7.jpg",
"piezas":2
},
{
"name":"ALHAJERO DE 10X15 CM - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":300.00,
"uuid":"3005",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 8.jpg",
"piezas":10
},
{
"name":"ALHAJERO EN FORMA DE ROPERO - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":240.00,
"uuid":"3009",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 9.jpg",
"piezas":5
},
{
"name":"COLECCIÓN 7 INSTRUMENTOS - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":900.00,
"uuid":"30003",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 10.jpg",
"piezas":4
},
{
"name":"COLECCIÓN 5 INSTRUMENTOS - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":660.00,
"uuid":"3004",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 11.jpg",
"piezas":5
},
{
"name":"JUEGO DE DOMINÓ - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":1020.00,
"uuid":"3002",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 12.jpg",
"piezas":5
},
{
"name":"PORTA-RETRATO ESPECIAL - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":360.00,
"uuid":"3018",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 13.jpg",
"piezas":4
},
{
"name":"PORTA-RETRATO SENCILLO - CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":300.00,
"uuid":"3017",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 14.jpg",
"piezas":9
},
{
"name":"COJÍN TENANGO",
"description":"BORDADO A MANO CON HILO DE ALGODÓN MULTICOLOR",
"price":480.00,
"uuid":"7014",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 15.jpg",
"piezas":2 
},
{
"name":"COJÍN TENANGO",
"description":"BORDADO A MANO CON HILO DE ALGODÓN MULTICOLOR",
"price":480.00,
"uuid":"7014",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 16.jpg",
"piezas":2
},
{
"name":"BUFANDA NEGRA HILO ROJO",
"description":"TEJIDA EN TELAR DE CINTURA CON DISTINTIVOS DEL VALLE DEL MEZQUITAL",
"price":300.00,
"uuid":"1040",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 17.jpg",
"piezas":1
},
{
"name":"REBOZO LLENO",
"description":"ELABORADO EN TELAR DE CINTURA EN COLOR ROJO CON BLANCO",
"price":6000.00,
"uuid":"1039",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 18.jpg",
"piezas":1
},
{
"name":"REBOZO DE MANTA",
"description":"BORDADO A MANO CON DISTINTIVOS DEL VALLE DEL MEZQUITAL",
"price":600.00,
"uuid":"1021",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 19.jpg",
"piezas":6
},
{
"name":"BOLSA DE MANTA TENANGO",
"description":"BORDADA A MANO SOBRE MANTA, CON HILOS DE ALGODÓN MULTICOLOR",
"price":480.00,
"uuid":"7001",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 20.jpg",
"piezas":3
},
{
"name":"TORTILLERO",
"description":"BORDADO A MANO SOBRE MANTA, CON HILOS DE ALGODÓN COLOR ROJO",
"price":216.00,
"uuid":"7007",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 21.jpg",
"piezas":10
},
{
"name":"MORRAL DE MANTA",
"description":"BORDADO A MANO SOBRE MANTA, CON HILOS DE ALGODÓN MULTICOLOR",
"price":240.00,
"uuid":"7004",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 22.jpg",
"piezas":2
},
{
"name":"CAMINO DE MESA ANCHO",
"description":"BORDADO A MANO SOBRE MANTA, CON HILOS DE ALGODÓN COLOR AZUL CIELO",
"price":1140.00,
"uuid":"7006",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 23.jpg",
"piezas":4
},
{
"name":"CAJÓN DON JULIO",
"description":"ELABORADO EN MADERA CON ERRAJES DE HIERRO Y 4 VASOS TEQUILEROS EN VIDRIO SOPLADO (NO INCLUYE BOTELLA)",
"price":540.00,
"uuid":"15001",
"categoria":"Artesanías",
"subcategoria":"Madera y Herrería",
"image":"/images/products/Art 24.jpg",
"piezas":1
},
{
"name":"4 PACK MINILICORES",
"description":"LICOR DE 50 ML C/U SABORES DE ZARZAMORA Y XONOSTLE",
"price":216.00,
"uuid":"17006",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 25.jpg",
"piezas":3
},
{
"name":"LICOR MEDIANO 375 ML.",
"description":"LICOR DE 375 ML SABORES DE GRANAGA, JAMAÍCA, XOCONOSTLE Y ZARZAMORA",
"price":216.00,
"uuid":"17002",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 26.jpg",
"piezas":36
},
{
"name":"LICOR GRANDE 750 ML.",
"description":"LICOR DE 750 SABORES DE XOCONOSTLE, JAMAÍCA, ZARZAMORA",
"price":312.00,
"uuid":"17001",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 27.jpg",
"piezas":6
},
{
"name":"SALSA GRANDE",
"description":"CONT. 272 ML, SABORES DE  CHINICUILES, CHAPULINES, CHILTEPÍN Y SALSA DE CHICHARRAS",
"price":219.60,
"uuid":"17005",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 28.jpg",
"piezas":35
},
{
"name":"ADEREZO GRANDE",
"description":"CONT. 272 ML SABORES DE CAVIAR DE HORMÍGAS Y FINAS HIERBAS",
"price":219.60,
"uuid":"17004",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 29.jpg",
"piezas":6
},
{
"name":"MERMELADA GRANDE",
"description":"CONT.272 ML SABORES DE HIGO, JAMAÍCA, KIWI CON NOPAL Y XOCONOSTLE",
"price":114.00,
"uuid":"17003",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 30.jpg",
"piezas":17
},
{
"name":"8 PACK MINISALSA",
"description":"CONT. 45 ML SABORES DE CHILTEPÍN, CHINICUILES, PULQUE CON CARACOLES Y CHICHARRAS",
"price":432.00,
"uuid":"17008",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 31.jpg",
"piezas":3
},
{
"name":"CATRINA DE HOJA DE MAÍZ",
"description":"CATRINA ELABORADA EN HOJA DE MAIZ Y TEÑIDA CON TINTES NATURELES",
"price":156.00,
"uuid":"11001",
"categoria":"Artesanías",
"subcategoria":"Hoja de maíz",
"image":"/images/products/Art 32.jpg",
"piezas":2
},
{
"name":"ESFERA VITRO GRANDE",
"description":"ELABORADA EN CERÁMICA Y VIDRIOS DISTINTOS COLORES",
"price":1020.00,
"uuid":"14010",
"categoria":"Artesanías",
"subcategoria":"Cerámica y vitral",
"image":"/images/products/Art 33.jpg",
"piezas":1
},
{
"name":"PLATO HONDO GRANDE DE COBRE",
"description":"ELABORADO EN CERÁMICA VIDRIADA CON COBRE",
"price":384.00,
"uuid":"14031",
"categoria":"Artesanías",
"subcategoria":"Cerámica y vitral",
"image":"/images/products/Art 34.jpg",
"piezas":1
},
{
"name":"PLATO ORNAMENTAL CHICO",
"description":"ELABORADO EN CERÁMICA VIDRIADA CON COBRE",
"price":240.00,
"uuid":"14030",
"categoria":"Artesanías",
"subcategoria":"Cerámica y vitral",
"image":"/images/products/Art 35.jpg",
"piezas":1
},
{
"name":"RELOJ MONUMENTAL MEDIANO",
"description":"ELABORADO EN RECINA CON CANTERA MOLIDA",
"price":1404.00,
"uuid":"5002",
"categoria":"Artesanías",
"subcategoria":"Madera y Mármol",
"image":"/images/products/Art 36.jpg",
"piezas":1
},
{
"name":"CASCO MINERO RELOJ",
"description":"ELABORADO EN RECINA",
"price":240.00,
"uuid":"5003",
"categoria":"Artesanías",
"subcategoria":"Cobre",
"image":"/images/products/Art 37.jpg",
"piezas":1
},
{
"name":"MINERO CON GONDOLA RELOJ",
"description":"ELABORADO EN RECINA",
"price":420.00,
"uuid":"5004",
"categoria":"Artesanías",
"subcategoria":"Cobre",
"image":"/images/products/Art 38.jpg",
"piezas":1
},
{
"name":"ALHAJERO GRANDE SIN TAPA DE ESTRELLAS",
"description":"ELABORADO EN CERÁMICA VIDRIADA",
"price":528.00,
"uuid":"14032",
"categoria":"Artesanías",
"subcategoria":"Cerámica y vitral",
"image":"/images/products/Art 39.jpg",
"piezas":1
},
{
"name":"PLATO DE CERÁMICA",
"description":"ELABORADO EN CERÁMICA VIDRIADA",
"price":150.00,
"uuid":"14018",
"categoria":"Artesanías",
"subcategoria":"Cerámica y vitral",
"image":"/images/products/Art 40.jpg",
"piezas":4
},
{
"name":"PLATO DE BARRO CHICO POPOTILLO",
"description":"ELABORADO SOBRE PLATO DE BARRO CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":120.00,
"uuid":"8003",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 41.jpg",
"piezas":4
},
{
"name":"CAZUELA QUESERA POPOTILLO",
"description":"ELABORADO SOBRE PLATO DE BARRO CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":60.00,
"uuid":"8006",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 42.jpg",
"piezas":10
},
{
"name":"TAZA DE CERÁMICA",
"description":"ELABORADA EN CERÁMICA VIDRIADA",
"price":276.00,
"uuid":"14033",
"categoria":"Artesanías",
"subcategoria":"Cerámica",
"image":"/images/products/Art 43.jpg",
"piezas":1
},
{
"name":"ALHAJERO MADERA POPOTILLO",
"description":"ELABORADO EN MADERA CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":180.00,
"uuid":"8002",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 44.jpg",
"piezas":3 
},
{
"name":"PLATÓN GRANDE DE BARRO Y/O CERÁMICA POPOTILLO",
"description":"ELABORADO SOBRE PLATO DE BARRO CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":420.00,
"uuid":"8005",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 45.jpg",
"piezas":1
},
{
"name":"PLATO DE CERÁMICA MEDIANO CON PAÍSAJE",
"description":"ELABORADO SOBRE PLATO DE CERÁMICA CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":192.00,
"uuid":"8004",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 46.jpg",
"piezas":5
},
{
"name":"CUADRO 15X25",
"description":"ELABORADO SOBRE MADERA CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":300.00,
"uuid":"8010",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 47.jpg",
"piezas":5
},
{
"name":"CUADRO 28X35 POPOTILLO",
"description":"ELABORADO EN MADERA CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":2400.00,
"uuid":"8015",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 48.jpg",
"piezas":3
},
{
"name":"CARTERA BORDADA",
"description":"BORDADA A MANO CON DISTINTIVOS DEL VALLE DEL MEZQUITAL",
"price":204.00,
"uuid":"25001",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 49.jpg",
"piezas":8
},
{
"name":"PULSERA GRANDE",
"description":"ELABORADA EN FILIGRANA CON PIEDRA ….",
"price":156.00,
"uuid":"20004",
"categoria":"Artesanías",
"subcategoria":"Filigrana",
"image":"/images/products/Art 50.jpg",
"piezas":3
},
{
"name":"PULSERA CHICA",
"description":"ELABORADA EN FILIGRANA CON PIEDRA ….",
"price":84.00,
"uuid":"20003",
"categoria":"Artesanías",
"subcategoria":"Filigrana",
"image":"/images/products/Art 51.jpg",
"piezas":3
},
{
"name":"PULSERA ESLAVON TORSADO PLATA",
"description":"ELABORADA EN PLATA",
"price":780.00,
"uuid":"2032",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 52.jpg",
"piezas":1
},
{
"name":"PULSERA ESLAVON 1/2 PUNTO PLATA",
"description":"ELABORADA EN PLATA",
"price":540.00,
"uuid":"2033",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 53.jpg",
"piezas":1
},
{
"name":"PULSERA PUNTO PERUANO PLATA",
"description":"ELABORADA EN PLATA",
"price":840.00,
"uuid":"2031",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 54.jpg",
"piezas":1
},
{
"name":"ARETE PUNTO PERUANO PLATA",
"description":"ELABORADOS EN PLATA",
"price":360.00,
"uuid":"2020",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 55.jpg",
"piezas":2
},
{
"name":"ARETE SERPENTINA PLATA",
"description":"ELABORADOS EN PLATA",
"price":300.00,
"uuid":"2025",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 56.jpg",
"piezas":2
},
{
"name":"ARETE CORAZÓN PLATA",
"description":"ELABORADOS EN PLATA",
"price":264.00,
"uuid":"2026",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 57.jpg",
"piezas":2
},
{
"name":"ARETE PAULINA PLATA",
"description":"ELABORADOS EN PLATA",
"price":204.00,
"uuid":"2010",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 58.jpg",
"piezas":1
},
{
"name":"ARETE CARACOL GRANDE PLATA",
"description":"ELABORADOS EN PLATA",
"price":324.00,
"uuid":"2001",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 59.jpg",
"piezas":1
},
{
"name":"ARRACADA DE GRECAS PLATA",
"description":"ELABORADOS EN PLATA",
"price":276.00,
"uuid":"2004",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 60.jpg",
"piezas":1
},
{
"name":"ARETE MINIATURA BORDADO",
"description":"ELABORADOS EN PLATA",
"price":54.00,
"uuid":"1026",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 61.jpg",
"piezas":7
},
{
"name":"ARETE BORDADO GRANDE",
"description":"ELABORADOS EN PLATA",
"price":54.00,
"uuid":"1026",
"categoria":"Artesanías",
"subcategoria":"Plata",
"image":"/images/products/Art 62.jpg",
"piezas":7
},
{
"name":"LLAVERO DE IXTLE",
"description":"ELABORADO EN IXTLE NATURAL Y POSTERIORMENTE PIGMENTADO ",
"price":48.00,
"uuid":"13003",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 63.jpg",
"piezas":5
},
{
"name":"COLLAR Y ARETES",
"description":"PEDRERÍA UNIDA MEDIANTE HILOS",
"price":144.00,
"uuid":"24004",
"categoria":"Artesanías",
"subcategoria":"Fibras Vegetales",
"image":"/images/products/Art 64.jpg",
"piezas":3
},
{
"name":"JUEGO DE COLLAR ARETES Y PULSERA",
"description":"PEDRERÍA UNIDA MEDIANTE HILOS",
"price":168.00,
"uuid":"24005",
"categoria":"Artesanías",
"subcategoria":"Pedrería",
"image":"/images/products/Art 65.jpg",
"piezas":2
},
{
"name":"JUEGO DE COLLAR ARETES Y PULSERA",
"description":"PEDRERÍA UNIDA MEDIANTE HILOS",
"price":168.00,
"uuid":"24005",
"categoria":"Artesanías",
"subcategoria":"Pedrería",
"image":"/images/products/Art 66.jpg",
"piezas":2
},
{
"name":"MORRAL AZUL",
"description":"BORDADO SOBRE TELA",
"price":240.00,
"uuid":"1027",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 67.jpg",
"piezas":1
},
{
"name":"PORTA-LENTES",
"description":"BORDADO SOBRE TELA",
"price":144.00,
"uuid":"1005",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 68.jpg",
"piezas":6
},
{
"name":"PACHÓN DE PALMA",
"description":"PALMA NATURAL TEJIDA",
"price":600.00,
"uuid":"13007",
"categoria":"Artesanías",
"subcategoria":"Palma",
"image":"/images/products/Art 69.jpg",
"piezas":1
},
{
"name":"PAQUETE CHAROLERO DE 12",
"description":"BORDADO SOBRE TELA",
"price":1140.00,
"uuid":"1029",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 70.jpg",
"piezas":1
},
{
"name":"TOALLA ROJA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":156.00,
"uuid":"1038",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 71.jpg",
"piezas":1
},
{
"name":"JUEGO PARA BAÑO 2",
"description":"BORDADO SOBRE TELA",
"price":240.00,
"uuid":"1007",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 72.jpg",
"piezas":1
},
{
"name":"SERVILLETA GRANDE",
"description":"BORDADO SOBRE TELA",
"price":360.00,
"uuid":"1024",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 73.jpg",
"piezas":1
},
{
"name":"PORTA-CALIENTE",
"description":"BORDADO SOBRE TELA",
"price":84.00,
"uuid":"7008",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 74.jpg",
"piezas":7
},
{
"name":"BUFANDA MANTA BLANCA",
"description":"BORDADO SOBRE MANTA",
"price":300.00,
"uuid":"1020",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 75.jpg",
"piezas":6
},
{
"name":"BOLSA DE MANTA CON LONETA",
"description":"BORDADO SOBRE MANTA",
"price":480.00,
"uuid":"7002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 76.jpg",
"piezas":1
},
{
"name":"ALHAJERO DE 12X8 CM CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":204.00,
"uuid":"3006",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 78.jpg",
"piezas":9
},
{
"name":"ALHAJERODE 5X8 CM CONCHA DE ABULÓN",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":120.00,
"uuid":"3010",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 79.jpg",
"piezas":9
},
{
"name":"PASTILLERO EN FORMA DE OVALO",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":96.00,
"uuid":"3013",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 80.jpg",
"piezas":4
},
{
"name":"PASTILLERO CUADRADO",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":96.00,
"uuid":"3011",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 81.jpg",
"piezas":9
},
{
"name":"INSTRUMENTO CON ESTUCHE",
"description":"ELABORADO EN MADERA DE ENEBRO CON INCRUSTACIÓN DE CONCHA DE ABULÓN",
"price":96.00,
"uuid":"3001",
"categoria":"Artesanías",
"subcategoria":"Madera y Concha de abulón",
"image":"/images/products/Art 82.jpg",
"piezas":47
},
{
"name":"PALA DE MADERA GRANDE POPOTILLO",
"description":"ELABORADO SOBRE MADERA CON FIBRA VEGETAL DE POPOTILLO PIGMENTADO",
"price":120.00,
"uuid":"8007",
"categoria":"Artesanías",
"subcategoria":"Popotillo",
"image":"/images/products/Art 83.jpg",
"piezas":7
},
{
"name":"MINI-SALSAS INDIVIDUALES",
"description":"SALSA MINIATURA CONT. 45 ml. SABORES DE CHILTEPÍN, CHINICUIL, PULQUE Y CARACOL",
"price":54.00,
"uuid":"17009",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 84.jpg",
"piezas":29
},
{
"name":"MINI-MERMELADA INDIVIDUAL",
"description":"MERMELADA MINIATURA  CONT. 45 ml. SABORES DE XOCONOSTLE, HIGO  JAMAÍCA",
"price":50.40,
"uuid":"17010",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 85.jpg",
"piezas":10
},
{
"name":"MINI-LICOR INDIVIDUAL",
"description":"LICOR MINIATURA CONT. 50 ml. SABORES DE JAMAICA, XOCONOSTLE Y ZARZAMORA",
"price":57.60,
"uuid":"17011",
"categoria":"Artesanías",
"subcategoria":"Gastronomía",
"image":"/images/products/Art 86.jpg",
"piezas":10
},
{
"name":"CAMINO DE MESA ANGOSTO",
"description":"BORDADO SOBRE MANTA",
"price":540.00,
"uuid":"1008",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 87.jpg",
"piezas":1
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 88.jpg",
"piezas":27
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 89.jpg",
"piezas":27
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 90.jpg",
"piezas":27
},
{
"name":"MASCARA 950",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":1140.00,
"uuid":"6017",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 91.jpg",
"piezas":10
},
{
"name":"MASCARA 950",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":1140.00,
"uuid":"6017",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 92.jpg",
"piezas":10
},
{
"name":"MASCARA490",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":588.00,
"uuid":"6015",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 93.jpg",
"piezas":22
},
{
"name":"CALABERA AL NATURAL MADERA PEMUCHE",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":588.00,
"uuid":"6011",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 94.jpg",
"piezas":1
},
{
"name":"COMANCHE AL NATURAL",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":1140.00,
"uuid":"6005",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 95.jpg",
"piezas":1
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 96.jpg",
"piezas":27
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 97.jpg",
"piezas":27
},
{
"name":"MASCARA 490",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":588.00,
"uuid":"6015",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 98.jpg",
"piezas":22
},
{
"name":"MASCARA 950",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":1440.00,
"uuid":"6014",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 99.jpg",
"piezas":6
},
{
"name":"MASCARA 350",
"description":"MADERA TIPO PEMUCHE TALLADA PARA POSTERIORMENTE PINTARSE",
"price":420.00,
"uuid":"6016",
"categoria":"Artesanías",
"subcategoria":"Madera tallada",
"image":"/images/products/Art 100.jpg",
"piezas":27
},
{
"name":"OLLA DE BARRO CHICA",
"description":"BARRO MOLDEADO Y COCIDO",
"price":120.00,
"uuid":"22002",
"categoria":"Artesanías",
"subcategoria":"Barro cocido",
"image":"/images/products/Art 101.jpg",
"piezas":2
},
{
"name":"OLLA DE BARRO GRANDE",
"description":"BARRO MOLDEADO Y COCIDO",
"price":216.00,
"uuid":"22003",
"categoria":"Artesanías",
"subcategoria":"Barro cocido",
"image":"/images/products/Art 102.jpg",
"piezas":3
},
{
"name":"COMAL DE BARRO",
"description":"BARRO MOLDEADO Y COCIDO",
"price":180.00,
"uuid":"22003",
"categoria":"Artesanías",
"subcategoria":"Barro cocido",
"image":"/images/products/Art 103.jpg",
"piezas":1
},
{
"name":"CAMISA HOMBRE",
"description":"BARRO MOLDEADO Y COCIDO",
"price":420.00,
"uuid":"23012",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 104.jpg",
"piezas":6
},
{
"name":"BLUSA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":300.00,
"uuid":"23004",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 105.jpg",
"piezas":24
},
{
"name":"VESTIDO DE NIÑA",
"description":"BORDADO SOBRE TELA",
"price":300.00,
"uuid":"23010",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 106.jpg",
"piezas":5
},
{
"name":"BLUSA PUNTO DE CRUZ",
"description":"BORDADO SOBRE TELA",
"price":420.00,
"uuid":"23005",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 107.jpg",
"piezas":3
},
{
"name":"CAMISA HOMBRE",
"description":"BORDADO SOBRE TELA",
"price":420.00,
"uuid":"23012",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 108.jpg",
"piezas":6
},
{
"name":"BLUSA CUADRADA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":240.00,
"uuid":"23001",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 109.jpg",
"piezas":9
},
{
"name":"BLUSA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":300.00,
"uuid":"23004",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 110.jpg",
"piezas":24
},
{
"name":"BLUSA ESTRAPLE BORDADA",
"description":"BORDADO SOBRE TELA",
"price":300.00,
"uuid":"23002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 111.jpg",
"piezas":2
},
{
"name":"BLUSA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":300.00,
"uuid":"23004",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 112.jpg",
"piezas":24
},
{
"name":"CAMISA PARA NIÑA BORDADA",
"description":"BORDADO SOBRE TELA",
"price":360.00,
"uuid":"7010",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 113.jpg",
"piezas":2
},
{
"name":"CAMISA MUJER BORDADA",
"description":"BORDADO SOBRE TELA",
"price":660.00,
"uuid":"7009",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 114.jpg",
"piezas":3
},
{
"name":"GUAYABERA HOMBRE",
"description":"BORDADO SOBRE MANTA",
"price":720.00,
"uuid":"7012",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 115.jpg",
"piezas":3
},
{
"name":"CAMISA HOMBRE",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"7013",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 116.jpg",
"piezas":3
},
{
"name":"BLUSA MUJER",
"description":"BORDADO SOBRE TELA",
"price":660.00,
"uuid":"7009",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 117.jpg",
"piezas":3
},
{
"name":"BLUSA BORDADA PUNTO ENREDADO",
"description":"BORDADO SOBRE TELA",
"price":1440.00,
"uuid":"26002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 118.jpg",
"piezas":8
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"26006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 119.jpg",
"piezas":4
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"26006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 120.jpg",
"piezas":4
},
{
"name":"BLUSA TEJIDO PEPENADO",
"description":"BORDADO SOBRE TELA",
"price":480.00,
"uuid":"26005",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 121.jpg",
"piezas":10
},
{
"name":"BLUSA BORDADO HILVANO",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"26003",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 122.jpg",
"piezas":2
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":720.00,
"uuid":"26006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 123.jpg",
"piezas":4
},
{
"name":"BLUSA BORDADA PUNTO ENREDADO",
"description":"BORDADO SOBRE TELA",
"price":440.00,
"uuid":"26002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 124.jpg",
"piezas":8
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21003",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 125.jpg",
"piezas":8
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21003",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 126.jpg",
"piezas":8
},
{
"name":"QUETZQUEMETL TEJIDO CRUZADO",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21002",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 127.jpg",
"piezas":10
},
{
"name":"QUETZQUEMETL TEJIDO CRUZADO",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21002",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 128.jpg",
"piezas":10
},
{
"name":"QUETZQUEMETL TEJIDO CRUZADO",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21002",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 129.jpg",
"piezas":10
},
{
"name":"QUETZQUEMETL DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21003",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 130.jpg",
"piezas":8
},
{
"name":"REBOZO DE ESTAMBRE",
"description":"ESTAMBRE TEJIDO",
"price":360.00,
"uuid":"26001",
"categoria":"Artesanías",
"subcategoria":"Estambre",
"image":"/images/products/Art 131.jpg",
"piezas":11
},
{
"name":"REBOZO DE ESTAMBRE",
"description":"ESTAMBRE TEJIDO",
"price":360.00,
"uuid":"26001",
"categoria":"Artesanías",
"subcategoria":"Estambre",
"image":"/images/products/Art 132.jpg",
"piezas":11
},
{
"name":"REBOZO DE ESTAMBRE",
"description":"ESTAMBRE TEJIDO",
"price":360.00,
"uuid":"26001",
"categoria":"Artesanías",
"subcategoria":"Estambre",
"image":"/images/products/Art 133.jpg",
"piezas":11
},
{
"name":"REBOZO DE ESTAMBRE",
"description":"ESTAMBRE TEJIDO",
"price":360.00,
"uuid":"26001",
"categoria":"Artesanías",
"subcategoria":"Estambre",
"image":"/images/products/Art 134.jpg",
"piezas":11
},
{
"name":"REBOZO DE ESTAMBRE",
"description":"ESTAMBRE TEJIDO",
"price":360.00,
"uuid":"26001",
"categoria":"Artesanías",
"subcategoria":"Estambre",
"image":"/images/products/Art 135.jpg",
"piezas":11
},
{
"name":"CHAL DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21007",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 136.jpg",
"piezas":7
},
{
"name":"GABAN DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21004",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 137.jpg",
"piezas":2
},
{
"name":"VESTIDO CON FAJÍN",
"description":"BORDADO SOBRE TELA",
"price":600.00,
"uuid":"21001",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 138.jpg",
"piezas":1
},
{
"name":"VESTIDO BORDADO",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"27005",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 139.jpg",
"piezas":2
},
{
"name":"VESTIDO BORDADO",
"description":"BORDADO SOBRE TELA",
"price":720.00,
"uuid":"27005",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 140.jpg",
"piezas":2
},
{
"name":"PERUANO DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21005",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 141.jpg",
"piezas":3
},
{
"name":"PERUANO DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21005",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 142.jpg",
"piezas":3
},
{
"name":"PERUANO DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21005",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 143.jpg",
"piezas":3
},
{
"name":"CAPA DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 144.jpg",
"piezas":10
},
{
"name":"CAPA DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 145.jpg",
"piezas":10
},
{
"name":"BLUSÓN",
"description":"BORDADO SOBRE TELA",
"price":516.00,
"uuid":"27002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 146.jpg",
"piezas":3
},
{
"name":"BLUSÓN",
"description":"BORDADO SOBRE TELA",
"price":516.00,
"uuid":"27002",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 147.jpg",
"piezas":3
},
{
"name":"BLUSA CRUZADA MANGA 3/4",
"description":"BORDADO SOBRE TELA",
"price":480.00,
"uuid":"27004",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 148.jpg",
"piezas":2
},
{
"name":"BLUSA DOBLE BORDADO",
"description":"BORDADO SOBRE TELA",
"price":660.00,
"uuid":"27006",
"categoria":"Artesanías",
"subcategoria":"Bordado",
"image":"/images/products/Art 149.jpg",
"piezas":3
},
{
"name":"BUFANDA DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":72.00,
"uuid":"21001",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 150.jpg",
"piezas":24
},
{
"name":"CAPA DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":324.00,
"uuid":"21006",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 151.jpg",
"piezas":10
},
{
"name":"GABAN DE LANA",
"description":"LANA NATURAL TINTURADA Y TEJIDA",
"price":420.00,
"uuid":"21004",
"categoria":"Artesanías",
"subcategoria":"Lana",
"image":"/images/products/Art 152.jpg",
"piezas":2
}
]