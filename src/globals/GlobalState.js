export default ({artesanias,libros,musica}) => {
    const categorias = ["Madera y Herrería","Hoja de maíz","Madera y Mármol","Cobre","Cerámica y vitral","Cerámica","Filigrana","Fibras Vegetales","Pedrería","Palma","Madera y Concha de abulón","Popotillo","Gastronomía","Madera tallada","Barro cocido","Estambre","Bordado","Lana"];
	const materiales = ["CONCHA DE ABULÓN","POPOTILLO","IXTLE","POPOTILLO","CANTERA","CANTERA Y COBRE","TENANGO BORDADO","HOJA DE MAÍZ","PLATA","MANTA BORDADA","TELA BORDADA"];

    const Reducer = (state = {
        categorias: {
            "ARTESANIAS": {
                productos: [...artesanias],
                subcategorias: [...categorias],
                materiales: [...materiales]
            },
            "LIBROS": {
                productos: [...libros],
                subcategorias: [...categorias],
                materiales: [...materiales]
            },
            "MUSICA": {
                productos: [...musica],
                subcategorias: [...categorias],
                materiales: [...materiales]
            },
            "TODO": {
                productos: [
                    ...artesanias, 
                    ...libros, 
                    ...musica
                ],
                subcategorias: [...categorias],
                materiales: [...materiales]

            }
        },
        selected_categoria: "ARTESANIAS",
        selected_sub_categoria: "",
        selected_material: "",
        lista_deseos: (JSON.parse(window.localStorage.getItem("lista_deseos")) ? JSON.parse(window.localStorage.getItem("lista_deseos")) : []),
        lista_de_compras: (JSON.parse(window.localStorage.getItem("lista_de_compras")) ? JSON.parse(window.localStorage.getItem("lista_de_compras")) : []),
    }, action) => {
        switch(action.type){
            case "GLOBAL_STATE":
                return {...state, ...action.data};
            default:
                return state;
        }
    };

    const Action = (data) => {
        return {
            type: "GLOBAL_STATE",
            data: data
        }
    };

    return {
        states: {
            stateGlobal: Reducer
        },
        actions: {
            setStateGlobal: Action
        }
    }
}