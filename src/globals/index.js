import Global from './GlobalState';

export default (args) => {
    const data = Global(args);
    return {
        states: {
            ...data.states
        },
        actions: {
            ...data.actions
        }
    }
}