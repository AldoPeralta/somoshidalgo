import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './views/Home';
import Contacto from './views/Contacto';
import Login from './views/Login';
import Checkout from './views/Deseos';
import Producto from './views/Product';
import Gastronomia from './views/Gastronomia';
import Carrito from './views/Carrito';
import Inicio from './views/Inicio';

class Router extends Component {

	render(){
		return (
			<Switch>
                <Route exact path={window.carpeta + "/"} component={Home}/>
                <Route path={window.carpeta + "/contacto"} component={Contacto}/>
                <Route path={window.carpeta + "/detail"} component={Producto}/>
                <Route path={window.carpeta + "/artesanias"} component={Inicio}/>
                <Route path={window.carpeta + "/gastronomia"} component={Gastronomia}/>
                <Route path={window.carpeta + "/libros"} component={Inicio}/>
                <Route path={window.carpeta + "/musica"} component={Inicio}/>
                <Route path={window.carpeta + "/carrito"} component={Carrito}/>
                <Route path={window.carpeta + "/checkout"} component={Checkout}/>
                <Route path={window.carpeta + "/login"} component={Login}/>
            </Switch>
		);
	}
};

export default Router;